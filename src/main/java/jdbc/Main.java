package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import util.FileUtil;

public class Main {

	public static String URL = 
			//kohustuslik jdbc, alamprotokoll,andemd faili, faili asukoht, 
			"jdbc:hsqldb:file:${user.home}/data/jdbc/db;shutdown=true";

	public static void main(String[] args) throws Exception {

		String statements = FileUtil.readFileFromClasspath("schema.sql");
		
		for (String statement : statements.split(";")) {
			//many whitespaces: \\s*
			if(statement.matches("\\s*")){
				//continue terminates the current iteration of the loop 
				//(the whole loop doesn't stop) 
				continue;
			}
			executeUpdate(statement);
		}
		
		
		List<Person> persons = new PersonDao().findAllPersons();
		System.out.println(persons);
		
		Person person = new PersonDao().findPersonForId(2L);
		System.out.println(person);
		
	}

	private static void executeUpdate(String sql) throws SQLException {
		try (Connection conn = DriverManager.getConnection(URL); Statement stmt = conn.createStatement()) {

			stmt.executeUpdate(sql);
			
		}
	}

}
