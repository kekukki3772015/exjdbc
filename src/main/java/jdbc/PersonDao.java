package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PersonDao {

	// Tuleb m��rata �htne koht kus URL'i hoitake
	public static String URL = "jdbc:hsqldb:file:${user.home}/data/jdbc/db;shutdown=true";

	public List<Person> findAllPersons() throws SQLException {

		List<Person> persons = new ArrayList<>();

		try (Connection conn = DriverManager.getConnection(URL); Statement stmt = conn.createStatement()) {

			String sql = "SELECT id, name, age FROM person";

			try (ResultSet rs = stmt.executeQuery(sql)) {
				while (rs.next()) {
					Person person = new Person(rs.getLong(1), rs.getString(2), rs.getInt(3));
					persons.add(person);
				}
			}
		}
		return persons;
	}

	public Person findPersonForId(Long id) {

		String sql = "SELECT id, name, age FROM person" + " WHERE id = ?";

		try (Connection conn = DriverManager.getConnection(URL); PreparedStatement ps = conn.prepareStatement(sql)) {

			ps.setLong(1, id);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return new Person(rs.getLong(1), rs.getString(2), rs.getInt(3));
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

}
